package com.iit.demo.web.rest;


import com.iit.demo.dto.MedicationDTO;
import com.iit.demo.service.MedicationService;
import com.iit.demo.util.CalculatorUtil;
import com.iit.demo.util.RestPreconditions;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;

/**
 * REST controller for managing Medication.
 */
@RestController
public class HealthCheck {

    @GetMapping("/health")
    public String  healthCheck(){

        return "is healthy!";
    }


}
